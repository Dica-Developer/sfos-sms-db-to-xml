## SFOS SMS DB TO XML
Convert an SMS csv file to a "SMS Backup & Restore" Android app XML import file.
MMS and their data are not recovered.

Columns are:

* date send, e.g. 1545235206, timestamp in seconds
* direction, e.g. 2, 1..received SMS; 2..send SMS
* message, e.g. How is going, SMS message text
* sender or receiver, e.g. +41234568901234567890, phone number depending on the type it is the receiver or the sender
* end of line marker, "end_of_record"


## SFOS commhistory Sqlite DB to CSV

1. get the file commhistory from the phone `~/.local/share/commhistory/commhistory.db`
2. `sqlite3 commhistory.db "select startTime, direction, freeText, remoteUid, 'end_of_record' from Events" > sms.csv`
3. `dart run bin/sfos_sms_db_to_xml.dart sms.csv > sms.xml`
4. copy the file on the phone
6. open "SMS Backup & Restore" app on the phone and select the file for restore

## SFOS MMS data recovery

This tool does not recover MMS data. But you can find all the data of every MMS in the folder `~/.local/share/commhistory/data`.

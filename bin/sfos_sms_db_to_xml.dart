import 'package:sfos_sms_db_to_xml/sfos_sms_db_to_xml.dart' as sfos_sms_db_to_xml;

void main(List<String> arguments) {
  sfos_sms_db_to_xml.convert(arguments[0]);
}

import 'package:grizzly_io/grizzly_io.dart';
import 'dart:io';
import 'dart:convert';

/**
sqlite3 commhistory.db "select startTime, direction, freeText, remoteUid, 'end_of_record' from Events" > sms.csv
*/
convert(String csvFile) async {
  var file = File(csvFile);
  if (!await file.exists()) throw Exception('File not found!');
  var csv = file.readAsString(encoding: utf8);

  print("?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
  print('<smses count="2" backup_set="207496f0-25cc-4ed1-ad0e-0134aa3f0c5c" backup_date="1625493212385"·type="full">');
  for (final line in (await csv).split('|end_of_record')) {
    final columns = line.split('|');
    if (columns.length > 3) {
      final date = columns[0]; // in seconds
      final direction = columns[1]; // 1..received SMS; 2..send SMS
      final text = columns[2];
      final sender = columns[3];
      if (text.trim().length > 0) {
        if (direction == '1') {
          print('<sms protocol="0" address="${sender}" date="${int.parse(date) * 1000}" body="${text}" type="${direction}" date_sent="${int.parse(date) * 1000}" sub_id="2" subject="null" status="-1" service_center="+491760000470" read="1" toa="null" sc_toa="null" />');
        } else {
          print('<sms protocol="0" address="${sender}" date="${int.parse(date) * 1000}" body="${text}" type="${direction}" date_sent="0" sub_id="2" subject="null" status="-1" service_center="null" read="1" toa="null" sc_toa="null" />');
        }
      }
    }
  }
  print('</smses>');
}
